### This project is built as a selection task for Next Technologies Django Job Profile. It's an employee management system with login/signup functionality and employee management functionality.

## How to Use this app (Locally):
- Open this project in VS Code or open downloaded directory in terminal.
- Navigate to Project directory containing requirements.txt and manage.py file. (In this case, it's ems/)
- Run ```pip install -r requirements.txt```
- Once the installation of libraries is finished, run ```python manage.py runserver```
- You will get the following output in terminal:
    ```
    Django version 3.1, using settings 'ems.settings'
    Starting development server at http://127.0.0.1:8000/
    Quit the server with CONTROL-C.
    ```

- Now, open your browser and open URL: '127.0.0.1:8000/'
- In case of any issue, please set DEBUG=False and make ALLOWED_HOSTS list empty.

### This project is hosted on PythonAnyWhere and can be accessed by going on following URL:
    http://rohit3345.pythonanywhere.com




